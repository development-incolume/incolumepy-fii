from setuptools import setup

setup(
    name='incolumepy_fii',
    version='0.0.0.dev0',
    packages=[''],
    install_requires=[
        'setuptools',
        # -*- Extra requirements: -*-
        "bs4",
        "html5lib",
        "lxml",
        "openpyxl",
        "pandas",
        "requests",
    ],
    url='https://gitlab.com/development-incolume/incolumepy-fii',
    license='',
    author='@britodfbr',
    author_email='contato@incolume.com.br',
    description='Planilha Atualizada de FII - Fundo de Investimento Imobiliário',
    entry_points={
        'console_scripts': [
            'updatedsheet = incolumepy_fii.extract_fii:extract_fii_list',
        ],
    },
    # Get more strings from http://www.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
    ]
)
