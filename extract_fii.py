import os
import requests
from bs4 import BeautifulSoup
import pandas as pd
from unicodedata import normalize
from datetime import datetime
from time import sleep, time
from openpyxl import load_workbook


def epoch():
    return F'{datetime.now().strftime("%s")}'


def extract_fii_yeild_list(url='https://www.fundsexplorer.com.br/rendimentos-e-amortizacoes', ndays=2):
    file_xlsx = 'ranking-yeild.xlsx'
    req = requests.get(url)
    # print(req.status_code)
    soup = BeautifulSoup(req.content, 'html.parser')
    with open('ranking.html', 'wb') as f:
        f.write(soup.prettify(encoding='iso8859-1'))
    title = [x.text for x in soup.select('thead>tr>th')]
    print(title)
    body = [x.text for x in soup.select('tbody>tr>td')]
    print(body)
    df = pd.read_html('ranking.html')[-1]
    print(df.head())
    # print(df.columns)
    df.columns = [normalize('NFKD', x).encode('ASCII', 'ignore').decode('ASCII').lower() for x in df.columns]
    df.columns = df.columns.str.replace(r'\.|\(|\)', '')
    df.columns = df.columns.str.replace(r'\/| |^ +| +$', '_')
    df.columns = df.columns.str.replace(r"_+", "_")
    # print(df.columns)
    df.sort_values(by=['codigo_do_fundo'], ascending=True, inplace=True)
    # pd.to_numeric(df.dividend_yield)
    selecteds = ['codigo_do_fundo', 'setor', 'preco_atual',
                 'dividendo', 'dividend_yield', 'rentab_acumulada', 'dy_12m_media']
    filtro1 = df[selecteds]
    filtro1.preco_atual = filtro1.preco_atual.str.replace(r"R\$ |\.", '').str.replace(',', '.')
    filtro1.dividendo = filtro1.dividendo.str.replace(r"R\$ |\.", '').str.replace(',', '.')
    filtro1.dividend_yield = filtro1.dividend_yield.str.replace(',', '.').str.replace('%', '')
    filtro1.rentab_acumulada = filtro1.rentab_acumulada.str.replace(',', '.').str.replace('%', '')
    filtro1.dy_12m_media = filtro1.dy_12m_media.str.replace(',', '.').str.replace('%', '')
    # filtro1.astype({'preco_atual': 'float32', 'dividendo': 'float32',
    #                 'dividend_yield': 'float32', 'rentab_acumulada': 'float32', 'dy_12m_media': 'float32'}).dtypes
    filtro1.dy_12m_media = pd.to_numeric(filtro1.dy_12m_media)
    # filtro1.to_excel(file_xlsx, sheet_name=epoch())
    for i in selecteds[2:]:
        filtro1[i] = pd.to_numeric(filtro1[i])
    for i in selecteds[4:]:
        filtro1[i] = filtro1[i].apply(lambda n: n / 100)
    print(filtro1.head())
    print(filtro1.dtypes)

    # Se arquivo não existir ou for superior a 5 dias, criar novo
    if not os.path.isfile(file_xlsx) or os.stat(os.path.join(file_xlsx)).st_mtime < time() - ndays * 86400:
        df.to_excel(file_xlsx, sheet_name=epoch(), index=False)

    with pd.ExcelWriter(file_xlsx, engine='openpyxl') as writer:
        book = load_workbook(file_xlsx)
        writer.book = book
        sleep(1)
        filtro1.sort_values(by=['rentab_acumulada'], ascending=False).to_excel(writer, sheet_name=epoch(), index=False)


def extract_fii_list(url="https://www.fundsexplorer.com.br/ranking", ndays=2):
    file_xlsx = 'ranking.xlsx'
    req = requests.get(url)
    soup = BeautifulSoup(req.content, 'html.parser')
    table = soup.find('table')
    # print(table)
    df = pd.read_html(str(table))[-1]
    columns = ['codigo_do_fundo', 'setor', 'preco_atual', 'liquidez_diaria',
               'dividendo', 'dividend_yield', 'dy_3m_acumulado', 'dy_6m_acumulado',
               'dy_12m_acumulado', 'dy_3m_media', 'dy_6m_media', 'dy_12m_media', 'dy_ano',
               'variacao_preco', 'rentab_periodo', 'rentab_acumulada', 'patrimonio_liq',
               'vpa', 'p_vpa', 'dy_patrimonial', 'variacaopatrimonial',
               'rentab_patrno_periodo', 'rentab_patracumulada', 'vacancia_fisica',
               'vacancia_financeira', 'quantidade_ativos']
    df.columns = columns
    print(df.columns)
    selecteds = ['codigo_do_fundo', 'setor', 'preco_atual',
                 'dividendo', 'dividend_yield', 'rentab_acumulada', 'dy_12m_media', 'p_vpa']
    filtro1 = df[selecteds]
    filtro1.preco_atual = filtro1.preco_atual.str.replace(r"R\$ |\.", '').str.replace(',', '.')
    filtro1.dividendo = filtro1.dividendo.str.replace(r"R\$ |\.", '').str.replace(',', '.')
    filtro1.dividend_yield = filtro1.dividend_yield.str.replace(',', '.').str.replace('%', '')
    filtro1.rentab_acumulada = filtro1.rentab_acumulada.str.replace(',', '.').str.replace('%', '')
    filtro1.dy_12m_media = filtro1.dy_12m_media.str.replace(',', '.').str.replace('%', '')

    d = {k: 'float32' for k in selecteds[2:]}
    filtro1 = filtro1.astype(d)
    for i in selecteds[4:]:
        filtro1[i] = filtro1[i].apply(lambda n: n / 100)
    print(filtro1.head())
    print(filtro1.dtypes)
    # Se arquivo não existir ou for superior a 5 dias, criar novo
    if not os.path.isfile(file_xlsx) or os.stat(os.path.join(file_xlsx)).st_mtime < time() - ndays * 86400:
        df.to_excel(file_xlsx, sheet_name=epoch(), index=False)

    with pd.ExcelWriter(file_xlsx, engine='openpyxl') as writer:
        book = load_workbook(file_xlsx)
        writer.book = book
        sleep(1)
        filtro1.sort_values(by=['rentab_acumulada'], ascending=False).to_excel(writer, sheet_name=epoch(), index=False)


if __name__ == '__main__':
    extract_fii_list()
